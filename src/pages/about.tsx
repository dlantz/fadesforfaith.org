import React from 'react';
import { config, useSpring } from 'react-spring';
import Img from 'gatsby-image';
import Layout from '../components/layout';
import { AnimatedBox } from '../elements';
import SEO from '../components/SEO';
import styled from 'styled-components';
import { graphql } from 'gatsby';
import { ChildImageSharp } from '../types';

const Emphasis = styled('span')`
  font-weight: bold;
  font-size: 1.3rem;
`;

const Blurb = styled('div')`
  margin: 1rem 1.5rem;
  padding: 1rem 2rem;
  background-color: ${(props) => props.theme.colors.shade};
  border-radius: 4px;
  box-shadow: 2px 3px 4px ${(props) => props.theme.colors.grey};
`;

const List = styled('ul')`
  padding-left: 0;
`;
const ListItem = styled('li')`
  margin: 1rem 0;
  padding: 0 1rem;
  font-size: 1.3rem;
  list-style-type: none;
  border-left: 3px solid ${(props) => props.theme.colors.primary};
`;

const ImageContainer = styled('div')`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

const AboutContent = styled('div')`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const PartnerImage = styled('a')`
  width: 400px;
  margin: auto;
  @media (max-width: ${(props) => props.theme.breakpoints[3]}) {
    margin-top: 5rem;
  }
`;

type PageProps = {
  data: {
    covenantHouseLogo: ChildImageSharp & { publicURL: string };
    orchardLogo: ChildImageSharp & { publicURL: string };
    content: any;
  };
};

const About: React.FunctionComponent<PageProps> = ({
  data: { covenantHouseLogo, orchardLogo, content },
}) => {
  const pageAnimation = useSpring({
    config: config.slow,
    from: { opacity: 0 },
    to: { opacity: 1 },
  });
  return (
    <Layout>
      <SEO
        title="About | Fades for Faith"
        desc="Fades for Faith is a 501(c)(3) non-profit organization that provides grooming services and opportunities for career development in barbering skills to underprivileged male youth in group and foster homes.
        "
      />
      <AnimatedBox
        style={pageAnimation}
        py={[6, 6, 6, 8]}
        px={[6, 6, 8, 6, 8, 13]}
      >
        <AboutContent dangerouslySetInnerHTML={{ __html: content.html }} />
        <h2>Partners</h2>
        <ImageContainer>
          <PartnerImage href="https://www.meetorchard.org/" target="_blank">
            <Img fluid={orchardLogo.childImageSharp.fluid}></Img>
          </PartnerImage>
          <PartnerImage href="https://www.covenanthouse.org/" target="_blank">
            <Img fluid={covenantHouseLogo.childImageSharp.fluid}></Img>
          </PartnerImage>
        </ImageContainer>
      </AnimatedBox>
    </Layout>
  );
};

export default About;

export const query = graphql`
  query About {
    content: markdownRemark(frontmatter: { title: { eq: "About" } }) {
      html
      headings {
        depth
        value
      }
      frontmatter {
        title
      }
    }
    covenantHouseLogo: file(
      sourceInstanceName: { eq: "images" }
      name: { eq: "covenant-house" }
    ) {
      publicURL
      childImageSharp {
        fluid(quality: 95, maxWidth: 400) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    orchardLogo: file(
      sourceInstanceName: { eq: "images" }
      name: { eq: "orchard" }
    ) {
      publicURL
      childImageSharp {
        fluid(quality: 95, maxWidth: 400) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`;
