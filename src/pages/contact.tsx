import React from 'react';
import Layout from '../components/layout';
import { AnimatedBox } from '../elements';
import SEO from '../components/SEO';
import styled from 'styled-components';
import FacebookIcon from '../svg/facebook.svg';
import Img from 'gatsby-image';
import { graphql } from 'gatsby';
import { ChildImageSharp } from '../types';

const Address = styled('address')`
  margin: 0 auto;
  text-align: center;
  font-style: normal;
`;

const IconContainer = styled('div')`
  padding-top: 2rem;
  display: flex;
  width: 150px;
  margin: auto;
  align-items: center;
  justify-content: space-between;
`;

const FB = styled(FacebookIcon)`
  height: 3rem;
  width: 3rem;
`;

const IG = styled(Img)`
  height: 4rem;
  width: 4rem;
`;

const Center = styled('div')`
  position: absolute;
  top: 40%;
  left: 50%;
  @media (max-width: ${(props) => props.theme.breakpoints[3]}) {
    left: 25%;
  }
`;

type Props = {
  data: {
    instagramLogo: ChildImageSharp & { publicURL: string };
  };
};
const Contact = ({ data: { instagramLogo } }: Props) => {
  return (
    <Layout>
      <SEO
        title="Contact | Fades for Faith"
        desc="Fades for Faith is a 501(c)(3) non-profit organization that provides grooming services and opportunities for career development in barbering skills to underprivileged male youth in group and foster homes.
        "
      />
      <Center>
        <Address>
          <h4>Kody Miller, Founder</h4>
          <p>404.604.9192</p>
          <a href="mailto:kody@fadesforfaith.org">kody@fadesforfaith.org</a>
        </Address>
        <IconContainer>
          <a
            href="https://www.facebook.com/pages/category/Nonprofit-Organization/Fades-For-Faith-107441114266071/"
            target="_blank"
          >
            <FB></FB>
          </a>
          <a href="https://www.instagram.com/_fadesforfaith/" target="_blank">
            <IG fluid={instagramLogo.childImageSharp.fluid}></IG>
          </a>
        </IconContainer>
      </Center>
    </Layout>
  );
};

export default Contact;

export const query = graphql`
  query Contact {
    instagramLogo: file(
      sourceInstanceName: { eq: "images" }
      name: { eq: "instagram" }
    ) {
      publicURL
      childImageSharp {
        fluid(quality: 95, maxWidth: 50) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`;
