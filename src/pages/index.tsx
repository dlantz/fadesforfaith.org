import React from "react";
import { graphql, Link } from "gatsby";
import Img from "gatsby-image";
import styled from "styled-components";
import { animated, useSpring, config } from "react-spring";
import Layout from "../components/layout";
import SEO from "../components/SEO";
import { ChildImageSharp } from "../types";

type PageProps = {
  data: {
    jumbotron: ChildImageSharp & { publicURL: string };
  };
};

const Area = styled(animated.div)`
  height: 100vh;
  display: flex;
  @media (max-width: ${(props) => props.theme.breakpoints[2]}) {
    height: 100%;
  }
`;

const Jumbotron = styled(Img)`
  position: absolute !important;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  height: 100vh;
  z-index: -1;
  &:before {
    position: absolute;
    z-index: 1;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    content: "";
    background-color: rgba(0, 0, 0, 0.4);
  }
`;

const MainContainer = styled("div")`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  justify-content: center;
`;

const Header = styled("h1")`
  color: white;
  text-align: center;
  align-self: center;
  font-weight: 300;
  font-size: 5rem;
  padding: 1rem;

  @media (max-width: ${(props) => props.theme.breakpoints[1]}) {
    font-size: 3rem;
  }
`;

const Button = styled(Link)`
  margin: 0 auto;
  padding: 1rem 1.5rem;
  border: none;
  border-radius: 2px;
  cursor: pointer;
  background-color: ${(props) => props.theme.colors.grey};
  text-decoration: none;
  &:hover {
    background-color: ${(props) => props.theme.colors.primary};
    color: ${(props) => props.theme.colors.shade};
  }
`;

const Index: React.FunctionComponent<PageProps> = ({ data: { jumbotron } }) => {
  const pageAnimation = useSpring({
    config: config.slow,
    from: { opacity: 0 },
    to: { opacity: 1 },
  });

  return (
    <Layout>
      <SEO banner={jumbotron.publicURL} />
      <Area style={pageAnimation}>
        <Jumbotron fluid={jumbotron.childImageSharp.fluid}></Jumbotron>
        <MainContainer>
          <Header>Empowerment Through Barbering</Header>
          <Button to="/about" aria-label="Read More">
            Read More
          </Button>
        </MainContainer>
      </Area>
    </Layout>
  );
};

export default Index;

export const query = graphql`
  query Index {
    jumbotron: file(
      sourceInstanceName: { eq: "images" }
      name: { eq: "kody-jumbotron" }
    ) {
      publicURL
      childImageSharp {
        fluid(quality: 95, maxWidth: 1200) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`;
