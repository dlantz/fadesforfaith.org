---
title: About
---

# What is Fades for Faith?

Fades for Faith is a 501(c)(3) non-profit organization that provides **grooming services** to men in recovery. We believe that through barbering, we can inspire confidence for men in recovery to **“Look Good, Feel Good, and Do Good.”**

Check out some of our work on [Facebook](https://www.facebook.com/pages/category/Nonprofit-Organization/Fades-For-Faith-107441114266071/), and please consider [donating](https://givebutter.com/V5VVWX)!

<img src="/media/kody.png" alt="Kody saying hello" style="width:50%;margin: 0 auto"/>
