module.exports = {
  pathPrefix: "/", // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"

  siteTitle: "Fades for Faith", // Navigation and Site Title
  siteTitleAlt: "Fades For Faith", // Alternative Site title for SEO
  siteTitleShort: "Fades for Faith", // short_name for manifest
  siteHeadline: "Empowerment through Barbering", // Headline for schema.org JSONLD
  siteUrl: "https://fadesforfaith.org", // Domain of your site. No trailing slash!
  siteLanguage: "en", // Language Tag on <html> element
  siteLogo: "/logos/logo.png", // Used for SEO and manifest
  siteDescription: "Empowering the Young Men of Atlanta",
  author: "dflantz", // Author for schema.org JSONLD

  // siteFBAppID: '123456789', // Facebook App ID - Optional
  userTwitter: "@fadesforfaith", // Twitter Username
  ogSiteName: "Fades for Faith", // Facebook Site Name
  ogLanguage: "en_US", // og:language
  googleAnalyticsID: "UA-XXXXXX-X",

  // Manifest and Progress color
  themeColor: "#db7436",
  backgroundColor: "#3b3c4f",
};
